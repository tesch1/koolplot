
all: kplot

libkoolplot.a: boss.o plotdata.o plotstream.o koolplot.o #bgi_util.o
	ar -rv libkoolplot.a $^

clean:
	rm *.o 
	rm *.exe 
	rm libkoolplot.a

SDL=-I/usr/include/SDL -DUSE_SDL1
SDL=-I/usr/include/SDL2 -DUSE_SDL2

CXX_FLAGS = -O2 -g -fno-rtti  -fno-common -fno-short-enums \
	-Wall -Wextra -Wno-unused-parameter \
	-pedantic-errors -DBOSS=1 -Iboss/include $(SDL) #-fno-exceptions

LINK_FLAGS = -lbgi -lgdi32 -lcomdlg32 -luuid -loleaut32 -lole32 -lstdc++ -lsupc++ 
LINK_FLAGS = -lSDL2 -lm -lSDL_ttf -lSDL_mixer

plotdata.o: plotdata.cxx plotdata.h
	$(CXX) $(CXX_FLAGS) -c  plotdata.cxx -o plotdata.o

boss.o: boss/src/boss.cpp boss/include/boss.h
	$(CXX) $(CXX_FLAGS) -c  boss/src/boss.cpp -o boss.o

plotstream.o: plotstream.cxx plotstream.h plotdata.h
	$(CXX) $(CXX_FLAGS) -c  plotstream.cxx -o plotstream.o

koolplot.o: koolplot.c koolplot.h plotstream.h plotdata.h
	$(CXX) $(CXX_FLAGS) -c  koolplot.c -o koolplot.o

#bgi_util.o: bgi_util.cpp bgi_util.h
#	$(CXX) $(CXX_FLAGS) -c  bgi_util.cpp -o bgi_util.o

kplot.o: kplot.c
	$(CXX) $(CXX_FLAGS) -c  kplot.c -o kplot.o

kplot: kplot.o libkoolplot.a
	$(CXX) ${CXX_FLAGS} -o kplot -s kplot.o libkoolplot.a $(LINK_FLAGS)


clean:
	rm *.o *.a
